#pragma once

#include <iostream>
#include <Windows.h>

//Sum 2 params and square them.
double SquareSum(int n1 = 0, int n2 = 0)
{
	return sqrt(n1 + n2);
}

//Sum 3 params and square them.
double SquareSum(int n1 = 0, int n2 = 0, int n3 = 0)
{
	return sqrt(n1 + n2 + n3);
}

//Math PowerOf base over exponent.
double PowerOf(int base, int exponent)
{
	return pow(base, exponent);
}

//Print odd or even numbers
void PrintOddEven(int N, bool isOdd)
{
	//0 to N;
	for (int i = 0; i < N; i++)
	{
		if (isOdd)
		{
			if (i % 2) std::cout << i << "\n";
		}
		else if (!isOdd)
		{
			if (!(i % 2)) std::cout << i << "\n";
		}
	}
}

//Get Current Day
int GetDay()
{
	SYSTEMTIME st;
	GetLocalTime(&st);
	return st.wDay;
}
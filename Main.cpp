#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;

//Animal class.
class Animal
{
public:
	virtual void Voice()
	{
		cout << "Animal Voice!" << endl;
	}
};

//Inherited classes
class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Woof!" << endl;
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "Meow!" << endl;
	}
};

class Bird : public Animal
{
public:
	void Voice() override
	{
		cout << "Tweat!" << endl;
	}
};


int main()
{
	//Create var and initialize it.
	Animal* AnimalArray[4] = { new Dog(), new Cat(), new Bird(), new Dog()};

	//print animal array
	for (auto element : AnimalArray)
	{
		element->Voice();
	}
}

